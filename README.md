Congratulations! You've made it to the project stage of our interview process.

Please fork this project and complete the challenges you'll find under the code_review and pattern_matching directories. When you've finished, please share your project with @forrestblount with access so that I can share with additional team members as needed.

If you have any questions or require more time to complete these challenges, don't hesitate to reach out.

<hr>

- Would you make any changes to the file naming or structure themselves? Why or why not? => **Issue Number #1**
- Can you improve the way data is being passed between these components? => **Issue Number #2**

The changes in the code have been commited in the commit **d132bab4d5599be2f3b2b23469bfe039bce8b4e1**, so you can see the differences there.

The pattern matching resolution is in pattern matching folder, under the name of **pattern_matching.rb**.