# I resolved this excersice with the recursive backtracking algorithm.
# I try to find the pattern iterating all along the possibilities and when
# I can't find a solution I return to the previous state and continue with the next value on the count.

def isPattern(input, pattern, matches)

  # Base case: if only one character remains in the pattern
  if pattern.size == 1
    # It checks if we find the solution.
    # I return true if I have found the solution, so I can continue testing the rest of the pattern.
    return !matches.include?(pattern) || matches[pattern] == input
  end

  (1..input.size - pattern.size + 1).each do |i|
    proposed_match = input[0..i - 1]

    # Will check if exists pattern in matches hash and compare with the proposed match.
    # If they are different, it's skip and compare the next substring with the key in matches.
    # Repeat until find the key in pattern is equal or the substring reach the max size of the input.
    next if matches.include?(pattern[0]) && matches[pattern[0]] != proposed_match

    # Assign the pattern char to the hash of matches (this is the "apply value with n" step).
    matches[pattern[0]] = proposed_match

    # Call the recursion to go forward and try to find a solution in this context.
    # If I can't find the solution, I have to remove the value and start again with the next pattern.
    ret = isPattern(input[i..-1], pattern[1..-1], matches)

    # If I found the solution, I return true.
    return true if ret == true

    # If I not found the solution, I have to remove the value to go back on the backtracking algorithm.
    matches.delete(pattern[0]) if matches.include?(pattern[0])
  end

  return false
end

puts isPattern("redblueredblue", "abab", {}) # Should return true
puts isPattern("asdasdasdasd", "aaaa", {}) # Should return true
puts isPattern("xyzabcxzyabc", "aabb", {}) # Should return false


# You can run this with irb and the path of this file. i.e. irb /user/home/pattern_matching.rb